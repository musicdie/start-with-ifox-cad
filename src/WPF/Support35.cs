﻿#if !NET45
using System.Windows;
namespace BaseFunc.WPF {
    /// <summary>
    /// 事件绑定标签类
    /// </summary>
    /// <seealso cref="System.Windows.Markup.MarkupExtension" />
    public class EventBindingExtension : MarkupExtension {
        /// <summary>
        /// 命令属性
        /// </summary>
        public string? Command { get; set; }
        /// <summary>
        /// 命令参数属性
        /// </summary>
        public string? CommandParameter { get; set; }
        /// <summary>
        /// 当在派生类中实现时，返回用作此标记扩展的目标属性值的对象。
        /// </summary>
        /// <param name="serviceProvider">可为标记扩展提供服务的服务提供程序帮助程序。</param>
        /// <returns>
        /// 要在应用了扩展的属性上设置的对象值。
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// </exception>
        public override object? ProvideValue(IServiceProvider serviceProvider) {
            return null;
        }

        private Type? GetEventHandlerType(MemberInfo memberInfo) {
            Type? eventHandlerType = null;
            if (memberInfo is EventInfo eventInfo) {
                //var info = memberInfo as EventInfo;
                //var eventInfo = info;
                eventHandlerType = eventInfo.EventHandlerType;
            } else if (memberInfo is MethodInfo methodInfo) {
                //var info = memberInfo as MethodInfo;
                //var methodInfo = info;
                ParameterInfo[] pars = methodInfo.GetParameters();
                eventHandlerType = pars[1].ParameterType;
            }

            return eventHandlerType;
        }

#pragma warning disable IDE0060 // 删除未使用的参数
        private object? CreateHandler(MemberInfo memberInfo, string cmdName, Type targetType)
#pragma warning restore IDE0060 // 删除未使用的参数
        {
            Type? eventHandlerType = GetEventHandlerType(memberInfo);

            if (eventHandlerType is null) return null;

            var handlerInfo = eventHandlerType.GetMethod("Invoke");
            var method = new DynamicMethod("", handlerInfo.ReturnType,
                new Type[]
                {
                    handlerInfo.GetParameters()[0].ParameterType,
                    handlerInfo.GetParameters()[1].ParameterType,
                });

            var gen = method.GetILGenerator();
            gen.Emit(OpCodes.Ldarg, 0);
            gen.Emit(OpCodes.Ldarg, 1);
            gen.Emit(OpCodes.Ldstr, cmdName);
            if (CommandParameter is null) {
                gen.Emit(OpCodes.Ldnull);
            } else {
                gen.Emit(OpCodes.Ldstr, CommandParameter);
            }
            gen.Emit(OpCodes.Call, getMethod);
            gen.Emit(OpCodes.Ret);

            return method.CreateDelegate(eventHandlerType);
        }

        static readonly MethodInfo getMethod = typeof(EventBindingExtension).GetMethod("HandlerIntern", new Type[] { typeof(object), typeof(object), typeof(string), typeof(string) });

#pragma warning disable IDE0051 // 删除未使用的私有成员
        static void Handler(object sender, object args)
#pragma warning restore IDE0051 // 删除未使用的私有成员
        {
            HandlerIntern(sender, args, "cmd", null);
        }
        /// <summary>
        /// Handlers the intern.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The arguments.</param>
        /// <param name="cmdName">Name of the command.</param>
        /// <param name="commandParameter">The command parameter.</param>
        public static void HandlerIntern(object sender, object args, string cmdName, string? commandParameter) {
            if (sender is FrameworkElement fe) {
                var cmd = GetCommand(fe, cmdName);
                object? commandParam = null;
                if (true) {
                    commandParam = GetCommandParameter(fe, args, commandParameter!);
                }
                if ((cmd is not null) && cmd.CanExecute(commandParam)) {
                    cmd.Execute(commandParam);
                }
            }
        }

        internal static ICommand? GetCommand(FrameworkElement target, string cmdName) {
            return null;
        }

        internal static object? GetCommandParameter(FrameworkElement target, object args, string commandParameter) {
            return null;
        }

        internal static ViewModelBase? FindViewModel(FrameworkElement? target) {
            return null;
        }

        internal static object? FollowPropertyPath(object target, string path, Type? valueType = null) {
            return null;
        }
    }
    /// <summary>
    /// ViewModel基类
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public class ViewModelBase : INotifyPropertyChanged {
        /// <summary>
        /// 属性值更改事件。
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;
        /// <summary>
        /// 属性改变时调用
        /// </summary>
        /// <param name="propertyName">属性名</param>
        public void OnPropertyChanged(string propertyName = "") {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        /// <summary>
        /// 设置属性函数，自动通知属性改变事件
        /// </summary>
        /// <typeparam name="T">属性类型</typeparam>
        /// <param name="storage">属性</param>
        /// <param name="value">属性值</param>
        /// <param name="propertyName">属性名</param>
        /// <returns>成功返回 <see langword="true"/>，反之 <see langword="false"/></returns>
        protected virtual bool Set<T>(ref T storage, T value, string propertyName = "") {
            if (object.Equals(storage, value)) return false;

            storage = value;
            this.OnPropertyChanged(propertyName);

            return true;
        }
        /// <summary>
        /// 创建命令
        /// </summary>
        /// <param name="executeMethod">要调用的命令函数委托</param>
        /// <returns>WPF命令</returns>
        protected RelayCommand CreateCommand(Action<object> executeMethod) {
            return CreateCommand(executeMethod, (o) => true);
        }
        /// <summary>
        /// 创建命令
        /// </summary>
        /// <param name="executeMethod">要调用的命令函数委托</param>
        /// <param name="canExecuteMethod">命令是否可以执行的委托</param>
        /// <returns>WPF命令</returns>
        protected RelayCommand CreateCommand(Action<object> executeMethod, Func<object, bool> canExecuteMethod) {
            return new RelayCommand(executeMethod, canExecuteMethod);
        }
    }

    /// <summary>
    /// 命令基类
    /// </summary>
    /// <seealso cref="System.Windows.Input.ICommand" />
    public class RelayCommand : ICommand {
        readonly Func<object, bool>? _canExecute;
        readonly Action<object> _execute;
        /// <summary>
        /// 初始化 <see cref="RelayCommand"/> 类.
        /// </summary>
        /// <param name="execute">执行函数</param>
        public RelayCommand(Action<object> execute) : this(execute, null) {

        }
        /// <summary>
        /// 初始化 <see cref="RelayCommand"/> 类.
        /// </summary>
        /// <param name="execute">执行函数委托</param>
        /// <param name="canExecute">是否可执行函数委托</param>
        /// <exception cref="ArgumentNullException">execute</exception>
        public RelayCommand(Action<object> execute, Func<object, bool>? canExecute) {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        /// <summary>
        /// 当出现影响是否应执行该命令的更改时发生。
        /// </summary>
        public event EventHandler CanExecuteChanged {
            add {
                if (_canExecute is not null) {
                    CommandManager.RequerySuggested += value;
                }
            }
            remove {
                if (_canExecute is not null) {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }
        /// <summary>
        /// 定义确定此命令是否可在其当前状态下执行的方法。
        /// </summary>
        /// <param name="parameter">此命令使用的数据。  如果此命令不需要传递数据，则该对象可以设置为 <see langword="null" />。</param>
        /// <returns>
        /// 如果可执行此命令，则为 <see langword="true" />；否则为 <see langword="false" />。
        /// </returns>
        [DebuggerStepThrough]
        public bool CanExecute(object parameter) {
            return _canExecute is null || _canExecute(parameter);
        }
        /// <summary>
        /// 定义在调用此命令时要调用的方法。
        /// </summary>
        /// <param name="parameter">此命令使用的数据。  如果此命令不需要传递数据，则该对象可以设置为 <see langword="null" />。</param>
        public void Execute(object parameter) {
            _execute(parameter);
        }
    }

    /// <summary>
    /// 命令泛型基类
    /// </summary>
    /// <typeparam name="T">事件类型</typeparam>
    /// <seealso cref="System.Windows.Input.ICommand" />
    public class RelayCommand<T> : ICommand {
        readonly Func<T, bool> _canExecute;
        readonly Action<T> _execute;
        /// <summary>
        /// 初始化 <see cref="RelayCommand{T}"/> 类。
        /// </summary>
        /// <param name="execute">执行函数</param>
        public RelayCommand(Action<T> execute) : this(execute, (o) => true) {

        }

        /// <summary>
        /// 初始化 <see cref="RelayCommand{T}"/> 类。
        /// </summary>
        /// <param name="execute">执行函数委托</param>
        /// <param name="canExecute">是否可执行函数委托</param>
        /// <exception cref="System.ArgumentNullException">execute</exception>
        public RelayCommand(Action<T> execute, Func<T, bool> canExecute) {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }
        /// <summary>
        /// 当出现影响是否应执行该命令的更改时发生。
        /// </summary>
        public event EventHandler CanExecuteChanged {
            add {
                if (_canExecute is not null) {
                    CommandManager.RequerySuggested += value;
                }
            }
            remove {
                if (_canExecute is not null) {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }
        /// <summary>
        /// 定义确定此命令是否可在其当前状态下执行的方法。
        /// </summary>
        /// <param name="parameter">此命令使用的数据。  如果此命令不需要传递数据，则该对象可以设置为 <see langword="null" />。</param>
        /// <returns>
        /// 如果可执行此命令，则为 <see langword="true" />；否则为 <see langword="false" />。
        /// </returns>
        public bool CanExecute(object parameter) {
            if (_canExecute is null) {
                return true;
            }
            return _canExecute((T)parameter);
        }
        /// <summary>
        /// 定义在调用此命令时要调用的方法。
        /// </summary>
        /// <param name="parameter">此命令使用的数据。  如果此命令不需要传递数据，则该对象可以设置为 <see langword="null" />。</param>
        public void Execute(object parameter) {
            if (_execute is not null && CanExecute(parameter)) {
                _execute((T)parameter);
            }
        }
    }

    /// <summary>
    /// 事件命令
    /// </summary>
    public class EventCommand : TriggerAction<DependencyObject> {
        /// <summary>
        /// 执行动作
        /// </summary>
        /// <param name="parameter">要执行的动作参数， 如果动作为提供参数，就设置为null</param>
        /// <summary>
        /// 事件
        /// </summary>
        public ICommand Command {
            get => Command;
            set { }
        }
        /// <summary>
        /// 事件属性
        /// </summary>
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(EventCommand), new PropertyMetadata(null));

        /// <summary>
        /// 事件参数，如果为空，将自动传入事件的真实参数
        /// </summary>
        public object CommandParameter {
            get { return CommandParameter; }
            set { }
        }
        /// <summary>
        /// 事件参数属性
        /// </summary>
        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(EventCommand), new PropertyMetadata(null));
    }

    public class TriggerAction<T> {
    }
}
#endif