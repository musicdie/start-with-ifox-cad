﻿/// autocad 引用
global using Autodesk.AutoCAD.ApplicationServices;
global using Autodesk.AutoCAD.Colors;
global using Autodesk.AutoCAD.DatabaseServices;
global using Autodesk.AutoCAD.EditorInput;
global using Autodesk.AutoCAD.Geometry;
global using Autodesk.AutoCAD.Runtime;
global using Autodesk.AutoCAD.DatabaseServices.Filters;
global using Autodesk.AutoCAD.GraphicsSystem;


global using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
global using LineWeight = Autodesk.AutoCAD.DatabaseServices.LineWeight;
global using Viewport = Autodesk.AutoCAD.DatabaseServices.Viewport;

global using IFoxCAD.Basal;
global using Autodesk.AutoCAD.GraphicsInterface;
global using Acap = Autodesk.AutoCAD.ApplicationServices.Application;
global using Group = Autodesk.AutoCAD.DatabaseServices.Group;
global using Registry = Microsoft.Win32.Registry;
global using RegistryKey = Microsoft.Win32.RegistryKey;
global using CursorType = Autodesk.AutoCAD.EditorInput.CursorType;
global using Cad_DwgFiler = Autodesk.AutoCAD.DatabaseServices.DwgFiler;
global using Cad_DxfFiler = Autodesk.AutoCAD.DatabaseServices.DxfFiler;
global using Cad_ErrorStatus = Autodesk.AutoCAD.Runtime.ErrorStatus;
/// ifoxcad
global using IFoxCAD.Cad;
global using static IFoxCAD.Cad.Env;
global using Microsoft.Win32;
/// 系统引用
global using System;
global using static System.Math;
global using System.Collections;
global using System.Collections.Generic;
global using System.ComponentModel;
global using System.IO;
global using System.Linq;
global using System.Reflection;
global using System.Text;
global using System.Text.RegularExpressions;
global using System.Runtime.InteropServices;
global using System.Web.Script.Serialization;
global using Exception = System.Exception;
global using System.Runtime.CompilerServices;
global using System.Diagnostics;
global using System.Windows.Input;
global using System.Reflection.Emit;
global using System.Windows.Markup;
global using System.Globalization;
global using System.Windows.Data;
global using System.Net;
global using System.Security.Cryptography;

#if NET45
global using BaseFunc.WPF;
#endif

