# StartWithIfoxCad

#### 介绍
本项目搭建了一个ifoxcad的初始运行环境，可供参考

#### 软件架构
软件架构说明


#### 安装教程

1.  在gitee克隆此仓库，这样就可以完全控制仓库的提交

    然后git clone [你的仓库地址]

2.  下载之后使用vs打开此项目会发现ifox子模块未加载如下图：

    ![子模块未加载](https://foruda.gitee.com/images/1669344678715582195/b789f2d8_1842824.png "屏幕截图")

    此时使用git命令跳转到assets文件夹，并执行以下命令：

    git clone -b jing https://gitee.com/inspirefunction/ifoxcad.git IFox

    注意：一个字母都不能错，必须在下载的项目中的assets文件夹下执行

    ![子模块加载步骤](https://foruda.gitee.com/images/1669345314917476067/121f22d5_1842824.png "屏幕截图")

3.  你可以随意修改MFC项目，并在此项目基础上在Features文件夹内进行自己的功能开发

    ![在Features中新建文件进行自己的功能开发](image.png)

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
